# frozen_string_literal:true

require_relative "board"
require_relative "ui"
# Tic-Tac-Toe Board
class Game
  include UI
  attr_accessor :player_one, :player_two, :board

  @player_one = nil
  @player_two = nil
  @current_player = nil

  def initialize
    @board = Board.new
  end

  def start
    @player_one = create_player("1")
    @player_two = create_player("2")

    render_board(board.cells)

    @current_player = player_one
    until board.full? || board.winner?
      play_turn(@current_player)
      update_current_player
    end
  end

  def game_over?
    winner(@current_player.name) if board.winner?
    draw if board.full?
  end

  def update_current_player
    @current_player = if @current_player.name == player_one.name
                        player_two
                      else
                        player_one
                      end
  end

  def create_player(id)
    prompt_name(id)
    player_two_name = gets.chomp
    clear_ui
    prompt_symbol(id)
    player_two_symbol = gets.chomp.split("").first
    clear_ui
    Player.new(id, player_two_name, player_two_symbol)
  end

  def play_turn(player)
    prompt_space
    selection = gets.chomp.to_i
    clear_ui
    if board.valid_selection?(selection)
      board.claim_space(selection, player.symbol)
    else
      prompt_new_space
    end
    render_board(board.cells)
    game_over?
  end
end
