# frozen_string_literal: true

# Tic-Tac-Toe Board
class Board
  attr_reader :cells

  WINNING_SELECTIONS = [
    [0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6],
    [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]
  ].freeze

  def initialize
    @cells = [1, 2, 3, 4, 5, 6, 7, 8, 9]
  end

  def claim_space(index, character)
    cells[index - 1] = character
  end

  def winner?
    WINNING_SELECTIONS.any? do |selection|
      [cells[selection[0]], cells[selection[1]], cells[selection[2]]].uniq.length == 1
    end
  end

  def valid_selection?(selection)
    (1..9).include?(selection) && cells[selection].is_a?(Integer)
  end

  def full?
    cells.all? do |cell|
      !(cell.is_a? Integer)
    end
  end
end
