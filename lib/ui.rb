# frozen_string_literal: true

# Module for rendering UI
module UI
  def prompt_name(player_number)
    puts "Enter a name for player #{player_number}"
  end

  def prompt_symbol(player_number)
    puts "Enter a symbol for player #{player_number}"
  end

  def prompt_space
    puts "Select a free space to play:"
  end

  def prompt_new_space
    puts "Invalid selection. Select a new space:"
  end

  def draw
    puts "Game over! It's a draw"
  end

  def winner(player_name)
    puts "Game over!"
    puts "#{player_name} won!"
  end

  def render_board(cells)
    puts <<~HEREDOC
       #{cells[0]} | #{cells[1]} | #{cells[2]}#{" "}
      ---+---+---
       #{cells[3]} | #{cells[4]} | #{cells[5]}#{" "}
      ---+---+---
       #{cells[6]} | #{cells[7]} | #{cells[8]}#{" "}
    HEREDOC
  end

  def clear_ui
    system "clear"
  end
end
