# frozen_string_literal:true

# Tic-Tac-Toe Player
class Player
  attr_reader :id, :name, :symbol

  def initialize(id, name, symbol)
    @id = id
    @name = name
    @symbol = symbol
  end
end
